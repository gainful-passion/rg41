## Week 1 Foundation Digging

## Digger arrived 
(2021-02-13 Sat)

![digger arrived!](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/digger_arrived.jpg)
(Digger arrived!)
-----

Week 1
## Day 1 
(2021-02-15)

Foundation work

Extension project started! Thrilled ad terrified.

All slabs moved to side, concrete base are broke up
-----

## Day 2 
(2021-02-16)

- The work shortly stop after the digger has some problem. The team left after 2 hours.

![day 2 - 1](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_2_1.jpg)

![day 2 - 2](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_2_2.jpg)

![day 2 - 3](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_2_3.jpg)
-----


## Day 3
(2021-02-17)

- Work resumed! Max bought a Bobcat dumper truck. This truck is facinating. The front and rear wheels axles are so close to each other, the front and rear wheels are almost touching each other. What that brings is the truck is very agile - negotiating tight corners, and I saw it actually spinned and squeezed to do a 90 degrees turn on spot.

![Bob Cat](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/bob_cat.jpg)

![day 3 - 1](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_3_1.jpg)

![day 3 - 2](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_3_2.jpg)
(There will be 3 grab loads to remove the soil.)
-----

## Day 4
(2021-02-18)

- Max carefully digged around the waste pipe that serves our house and neigbours. 
The foundation trenches are almost completed, 1.2 meters. The peg is for retaining the walls and making the depth.

![day 4 - 1 ](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_4_1.jpg)

![day 4 - 2 ](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_4_2.jpg)
(carefully worked around the waste pipe)

![day 4 - 3 ](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_4_3.jpg)

![day 4 - 4 ](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_4_4.jpg)

![day 4 - 5 ](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_4_5.jpg)
(I was there when the digger bucket accidentally hit the building foundation where I stood above, and it gave me a shock.)
-----

## Day 5
(2021-02-19)

- Digging work almost finish!. They really don't muck about.

![day 5 - 1](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_5_1.jpg)

![day 5 - 2](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_5_2.jpg)

![day 5 - 3](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_5_3.jpg)
-----

## Day 6
(2021-02-22 Monday)

- All quiet in the monning, but when they came, the cement mixer and all of them turned up at the same time!

![day 6 - 1](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_6_1.jpg)
The tanker carries cement, aggregator and water in seperate compartment and mixing on site to the specific amount.
The truck in front carries a concrete pump to pump it into the trenches via pipe.
1 hour from setup to compele. They have carefull flush clean the pipes before the cement residue set. 

![day 6 - 2](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_6_2.jpg)

![day 6 - 3](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_6_3.jpg)
8 cubic meters - 20 tons of concrete.
----

## Day 7
(2021-02-23 Tuesday)
![day 7 - 1](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_7_1.jpg)
![day 7 - 2](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_7_2.jpg)
----

## Day 8
(2021-02-24 Tuesday)

![day 8 - 1](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_8_1.jpg)
![day 8 - 2](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_8_2.jpg)
----

## Day 9
(2021-02-25)

![day 9 - 1](https://my-diy-photos.s3.eu-west-2.amazonaws.com/extension-foundation/day_9_1.jpg)
