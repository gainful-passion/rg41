# Loft Insulation

The property is an 1880 build edwardian style semi-detached house. 
This 2nd story loft room was orginally created when the propety was built.
The insulation is very poor, almost not existence.

Walls
Solid brick walls, which is impractical to insulate from inside, and unaesthetic if use cladding to cover the beautify period building features.
About 35% of heat are loss through solid walls 

Roof
- Pitched roof
- Solid brick walls at gables 
- Lime and lath plastering on ceiling, internal walls and eaves partition walls
- Partial insulated with 200mm fibre glass insulation roll at eaves 
About 25% of heat are loss through roof.

(A Lath is a narrow strip of wood which is nailed horizontally to each stud in the frame. A  traditional way of rendering lime plastering before plasterboard was invented.) 

![Original loft](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/loft_original.jpg)
(The open indoor space benefits from kids joyful visits on rainy days.)


## Phase 1 PIR insulation foam board and earthwool rolls

My plan was to keep orignal lime and lath plaster wall. 
- And to fill the stud walls from the back with PIR insulation board. 
- Fill the space between any horizontal joists (floor and ceiling) with 400mm earthwool.
- Fill the pitched roof cavity between rafters (depth 100mm) with 100mm earth wool rolls. 
However I only learned the lesson after most work had been done.

100mm earth wool inserted between the picthed rafters that directly under the bitumen felt leaves no space for ventilation. 
It will create condensation problem quickly.


![richard_cutting_stud_wall.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/richard_cutting_stud_wall.jpg)
(Richard cut open stud wall with sabre saw)

![open_stud_wall.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/open_stud_wall.jpg)
(opened stud wall)

![earthwool_in_joists.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/earthwool_in_joists.jpg)
earthwool filled between joists in eaves)

![loft_workspace.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/loft_workspace.jpg)
(keep the workspace tidy)


## Lesson learned - Ventilation! Ventilation!! Ventilation!!!

Pitched roof cavity between rafters should have a 50mm continuous void for air ventilation and to prevent condensation on the joist and roof felt.
This air channels between rafters should allow free flow of air via air vent.


ref. [roof air vents](https://www.fixmyroof.co.uk/roof-vents/) This article explains the challenges on insulating old house and fighting against damp moisture.

![roof air vent](https://www.fixmyroof.co.uk/wp-content/uploads/2015/05/Roof-ventilation.jpg)


## Phase 2 Total tear down

At this point, I decided to clean out all the lime plaster wall and replace them with PIR insulation foam board between and under the joists and then plasterboards to finish.
This is not a small job to say the least. I reckoned Richard, my handy man and I had removed around 2 tons of lime and dusts, and 1200 meters length of lath strips.

Taking down the lime plaster on Chinese New Years eve.

![taking_down_pitched_ceiling_1.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/taking_down_pitched_ceiling_1.jpg)
(Most daunting job - Knocking down ceilng plaster!)

![taking_down_pitched_ceiling_2.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/taking_down_pitched_ceiling_2.jpg)

![tear_down_1.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/tear_down_1.jpg)
(Rubble and dust! This is a war zone!)

![tear_down_2.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/tear_down_2.jpg)

![tear_down_3.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/tear_down_3.jpg)

![tear_down_4.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/tear_down_4.jpg)
(50+ bags)

![tear_down_5.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/tear_down_5.jpg)

![tear_down_6.jpg](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/tear_down_6.jpg)
(Completly tear down, leaving previous filled PIR board in place)

(^^ written 16/02/2021, updated 22/02/2021)

## Phase 3 
![loft insulation design](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/loft-insulation.jpg)
Loft insulation design - approved by building control with addition on roof air vents, and build stud walls to retail PIR to external walls.

![batten as anchoring point](https://my-diy-photos.s3.eu-west-2.amazonaws.com/loft-insulation/IMG_20210219_132341.jpg)

